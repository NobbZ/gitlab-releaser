package config

import (
	"fmt"
	"os"
	"strconv"

	"github.com/xanzy/go-gitlab"
	"go.uber.org/zap"
)

const (
	apiURLEnv     = "CI_API_V4_URL"
	glAPITokenEnv = "GL_TOKEN"
	poolSizeEnv   = "GL_POOLSIZE"
	projectIDEnv  = "CI_PROJECT_ID"
	projectURLEnv = "CI_PROJECT_URL"
	tagEnv        = "CI_COMMIT_TAG"

	poolSizeDefault = 4

	errorFmt = "API %v missing, please provide '%v' in the environment"
)

type config struct {
	apiURL         string // CI_API_V4_URL
	commitTag      string // CI_COMMIT_TAG
	gitlabAPIToken string // GL_TOKEN
	poolSize       int    // GL_POOLSIZE
	projectID      string // CI_PROJECT_ID
	projectURL     string // CI_PROJECT_URL
}

var conf = config{}

// ReadConfig consults the environment and reads the configuration.
func ReadConfig() error {
	var (
		apiURL, apiURLOk         = os.LookupEnv(apiURLEnv)
		commitTag, commitTagOk   = os.LookupEnv(tagEnv)
		glAPIToken, glAPITokenOk = os.LookupEnv(glAPITokenEnv)
		poolSize, poolSizeOk     = os.LookupEnv(poolSizeEnv)
		projectID, projectIDOk   = os.LookupEnv(projectIDEnv)
		projectURL, projectURLOk = os.LookupEnv(projectURLEnv)

		poolSizeInt = poolSizeDefault
	)

	if !apiURLOk {
		return fmt.Errorf(errorFmt, "URL", apiURLEnv)
	}

	if !commitTagOk {
		return fmt.Errorf(errorFmt, "commit tag", tagEnv)
	}

	if !glAPITokenOk {
		return fmt.Errorf(errorFmt, "token", glAPITokenEnv)
	}

	if !projectIDOk {
		return fmt.Errorf(errorFmt, "project ID", projectIDEnv)
	}

	if !projectURLOk {
		return fmt.Errorf(errorFmt, "project URL", projectURLEnv)
	}

	if poolSizeOk {
		parsed, err := strconv.ParseInt(poolSize, 10, 32)
		if err != nil {
			zap.L().Warn("Couldn't parse poolsize using default", zap.Error(err), zap.String("input", poolSize), zap.Int("default", poolSizeDefault))
		} else {
			poolSizeInt = int(parsed)
		}
	} else {
		zap.L().Info("No poolsize set, using default", zap.Int("default", poolSizeDefault))
	}

	conf.apiURL = apiURL
	conf.commitTag = commitTag
	conf.gitlabAPIToken = glAPIToken
	conf.poolSize = poolSizeInt
	conf.projectID = projectID
	conf.projectURL = projectURL

	return nil
}

// PoolSize returns the configured poolsize
func PoolSize() int {
	return conf.poolSize
}

func Tag() string {
	return conf.commitTag
}

func ProjectID() string {
	return conf.projectID
}

func ProjectURL() string {
	return conf.projectURL
}

func Client() (*gitlab.Client, error) {
	c := gitlab.NewClient(nil, conf.gitlabAPIToken)
	if err := c.SetBaseURL(conf.apiURL); err != nil {
		zap.L().Warn("change clients base URL", zap.Error(err))
		return nil, err
	}

	return c, nil
}
