package uploader

import (
	"fmt"

	"github.com/xanzy/go-gitlab"
	"go.uber.org/zap"

	"gitlab.com/NobbZ/gitlab-releaser/config"
)

func CreateOrUpdateRelease() error {
	var (
		tag         = config.Tag()
		pid         = config.ProjectID()
		client, err = config.Client()
	)

	if err != nil {
		zap.L().Error("can't fetch client", zap.Error(err))
		return err
	}

	releaseService := client.Releases
	release, _, err := releaseService.GetRelease(pid, tag)

	if release != nil {
		zap.L().Info("release already exists", zap.String("tag", tag), zap.Any("release", release))
		return nil
	}

	zap.L().Warn("No release returned from API (if error is 403, its fine)", zap.Error(err))

	description := fmt.Sprintf("Autorelease for %v", tag)

	zap.L().Info("creating new release", zap.String("tag", tag), zap.String("description", description))

	options := &gitlab.CreateReleaseOptions{
		Name:        &tag,
		TagName:     &tag,
		Description: &description,
	}

	release, _, err = releaseService.CreateRelease(pid, options)
	if err != nil {
		zap.L().Error("An error occured while creating the release", zap.String("tag", tag), zap.Error(err))
		return err
	}

	zap.L().Info("release successfully created", zap.String("tag", tag), zap.String("description", description), zap.Any("release", release))

	return nil
}
