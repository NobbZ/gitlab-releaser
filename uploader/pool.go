package uploader

import (
	"os"
	"path/filepath"
	"sync"
	"time"

	"github.com/xanzy/go-gitlab"
	"go.uber.org/zap"

	"gitlab.com/NobbZ/gitlab-releaser/config"
)

// UploadFiles will upload all files to gitlab.
//
// To do so, a pool is used with the configured poolsize to achieve parallel uploads.
func UploadFiles(files []string) {
	var (
		poolSize = config.PoolSize()

		jobs = make(chan string, poolSize*2)

		wg      = new(sync.WaitGroup)
		stopper = new(sync.WaitGroup)
	)

	wg.Add(len(files))

	for w := 0; w < poolSize; w++ {
		stopper.Add(1)
		go uploadWorker(w, jobs, wg, stopper)
	}

	for _, file := range files {
		jobs <- file
	}
	wg.Wait()
	close(jobs)
	stopper.Wait()
}

func uploadWorker(id int, files <-chan string, wg, stop *sync.WaitGroup) {
	logger := zap.L().With(zap.Int("worker", id))

	logger.Info("Worker started")

	for file := range files {
		logger.Info("Starting to upload file", zap.String("file", file))
		projFile, err := uploadFile(logger, file)
		if err != nil {
			logger.Warn("there was an error uploading the file", zap.Error(err), zap.String("file", file))
			wg.Done()
			continue
		}

		err = linkFile(logger, file, projFile)
		if err != nil {
			logger.Warn("there was an error linking ")
		}
		logger.Info("Finished uploading file", zap.String("file", file))
		wg.Done()
	}

	logger.Info("Worker quit")
	stop.Done()
}

func uploadFile(logger *zap.Logger, file string) (*gitlab.ProjectFile, error) {
	var (
		tag         = config.Tag()
		pid         = config.ProjectID()
		pwd, _      = os.Getwd()
		path        = filepath.Join(pwd, "files", file)
		client, err = config.Client()
	)

	logger = logger.With(zap.String("tag", tag), zap.String("file", file), zap.String("full_path", path))

	if err != nil {
		logger.Warn("error while fetching gitlab client, abort uploading the file", zap.Error(err))
		return nil, err
	}

	projectService := client.Projects
	logger.Info("Starting upload")
	start := time.Now()

	projFile, _, err := projectService.UploadFile(pid, path)
	if err != nil {
		logger.Warn("Upload failed", zap.Error(err))
		return nil, err
	}

	logger.Info("uploaded successfully", zap.Duration("duration", time.Since(start)), zap.Any("project_file", projFile))

	return projFile, nil
}

func linkFile(logger *zap.Logger, name string, data *gitlab.ProjectFile) error {
	var (
		tag      = config.Tag()
		pid      = config.ProjectID()
		url      = config.ProjectURL() + data.URL
		linkOpts = &gitlab.CreateReleaseLinkOptions{
			Name: &name,
			URL:  &url,
		}
		client, err = config.Client()
	)

	logger.With(zap.String("tag", tag), zap.String("file", name), zap.Any("project_file", data))

	if err != nil {
		logger.Warn("error while fetching gitlab client, abort linking the file", zap.Error(err))
	}

	releaseLinkService := client.ReleaseLinks

	link, _, err := releaseLinkService.CreateReleaseLink(pid, tag, linkOpts)
	if err != nil {
		logger.Warn("error while linking uploaded file", zap.Error(err), zap.String("file", name), zap.Any("project_file", data), zap.Any("link_opts", linkOpts))
		return err
	}
	logger.Info("linked successfully", zap.Any("link", link))
	return nil
}
