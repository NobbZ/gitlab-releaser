package main

import (
	"io/ioutil"
	"os"

	"go.uber.org/zap"

	"gitlab.com/NobbZ/gitlab-releaser/config"
	"gitlab.com/NobbZ/gitlab-releaser/uploader"
)

const version = "0.1.0"

func init() {
	zapConf := zap.NewProductionConfig()
	zapConf.Level = zap.NewAtomicLevelAt(zap.DebugLevel)

	logger, err := zapConf.Build()
	if err != nil {
		zap.L().Panic("Can't create logger", zap.Error(err))
	}

	zap.ReplaceGlobals(logger)
}

func main() {
	zap.L().Info("Starting gitlab-releaser", zap.String("version", version))
	zap.L().Info("Reading configuration from environment")

	err := config.ReadConfig()
	if err != nil {
		zap.L().Error("An error occured while reading configuration", zap.Error(err))
		os.Exit(1)
	}
	zap.L().Info("Read config successfully")

	zap.L().Info("Searching for files")
	fileInfos, err := ioutil.ReadDir("./files")
	if err != nil {
		zap.L().Error("An error occured while searching for files", zap.Error(err))
		os.Exit(1)
	}
	zap.L().Info("Found candidates", zap.Int("candidate_count", len(fileInfos)))
	files := make([]string, 0, len(fileInfos))
	for _, file := range fileInfos {
		if file.IsDir() {
			zap.L().Warn("Found directory in 'files', will not recurse nor upload", zap.String("directory", file.Name()))
			continue
		}
		files = append(files, file.Name())
	}
	zap.L().Info("Found files", zap.Strings("files", files))

	err = uploader.CreateOrUpdateRelease()
	if err != nil {
		zap.L().Error("Error while creating the release", zap.Error(err))
		os.Exit(1)
	}

	uploader.UploadFiles(files)

	zap.L().Info("Bye")
}
